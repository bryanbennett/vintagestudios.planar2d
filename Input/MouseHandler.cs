﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using VintageStudios.Planar2D.GameStructure;
using VintageStudios.Planar2D.Visuals.UI;

namespace VintageStudios.Planar2D.Input
{
    public class MouseHandler
    {
        private GameState parentState;
        private MouseState oldState;
        private List<VisualComponent> components;
        private int count;
        private VisualComponent clickedComponent;
        private bool hasClickedComponent;
        private int dragTime = 0;
        private bool dragging = false;
        private bool click = false;

        /// <summary>
        /// Returns the parent GameState.
        /// </summary>
        public GameState Parent
        {
            get
            {
                return parentState;
            }
        }
        public bool Clicked
        {
            get
            {
                return click;
            }
            set
            {
                click = value;
            }
        }
        public bool HasClickedComponent
        {
            get
            {
                return hasClickedComponent;
            }
            set
            {
                hasClickedComponent = true;
            }
        }

        public MouseHandler(GameState parentState)
        {
            oldState = Mouse.GetState();
            components = new List<VisualComponent>();
            this.parentState = parentState;
        }
        /// <summary>
        /// Adds a clickable component to this mouse handler.
        /// </summary>
        /// <param name="clickableComponent"></param>
        public void AddComponent(VisualComponent clickableComponent)
        {
            components.Add(clickableComponent);
            count = components.Count;
        }
        /// <summary>
        /// Removes a clickable component from this mouse handler.
        /// </summary>
        /// <param name="clickableComponent"></param>
        public void RemoveComponent(VisualComponent clickableComponent)
        {
            components.Remove(clickableComponent);
            count = components.Count;
        }
        public void Update(int delta)
        {
            MouseState newState = Mouse.GetState();
            click = false;
            if (oldState.LeftButton == ButtonState.Pressed && newState.LeftButton == ButtonState.Released)
            {
                dragging = false;

                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(newState.X, newState.Y) && entry.Visible)
                    {
                        entry.onRelease(newState);
                        hasClickedComponent = false;
                        clickedComponent = null;
                        break;
                    }
                    else
                    {
                        entry.Reset();
                    }
                    entry.Update(delta);
                }
            }
            else if ((oldState.LeftButton == ButtonState.Released && newState.LeftButton == ButtonState.Pressed) || (oldState.RightButton == ButtonState.Released && newState.RightButton == ButtonState.Pressed))
            {
                click = true;
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(newState.X, newState.Y) && entry.Visible)
                    {
                        entry.onClick(newState);
                        hasClickedComponent = true;
                        clickedComponent = entry;
                    }
                    else
                    {
                        entry.Reset();
                    }
                    entry.Update(delta);
                }
            }
            else if (oldState.LeftButton == ButtonState.Pressed && newState.LeftButton == ButtonState.Pressed)
            {
                if (hasClickedComponent)
                {
                    dragTime += delta;
                    if (dragTime >= 25 || dragging)
                    {
                        dragTime = 0;
                        dragging = true;
                        clickedComponent.onDrag(newState);
                        clickedComponent.Update(delta);
                    }
                }
            }
            else
            {
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(newState.X, newState.Y) && entry.Visible)
                    {
                        entry.onHover(newState);
                    }
                    else
                    {
                        entry.Reset();
                    }
                    entry.Update(delta);
                }
            }
            oldState = newState;
        }

    }
}
