﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VintageStudios.Shielder
{
    /// <summary>
    /// Exception class used to flag read errors from possible invalid Shielder Packages.
    /// </summary>
    public class InvalidShielderPackageException : Exception
    {
        public InvalidShielderPackageException()
        {
        }
        public InvalidShielderPackageException(string message)
            :base(message)
        {
        }
        public InvalidShielderPackageException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
