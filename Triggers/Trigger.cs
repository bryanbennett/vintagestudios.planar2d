﻿
namespace VintageStudios.Planar2D.Triggers
{
    public abstract class Trigger
    {
        public Trigger()
        {

        }
        /// <summary>
        /// Method that fires off when the condition is met.
        /// </summary>
        /// <returns></returns>
        public abstract void Event();
        /// <summary>
        /// Returns the trigger to its original state.
        /// </summary>
        public abstract void Reset();
        /// <summary>
        /// Primary method for determining if a condition is met.
        /// If the condition is met, the overwritten Update() method
        /// should call Event() and afterwards it should call Reset();
        /// </summary>
        /// <param name="delta"></param>
        public abstract void Update(int delta);
    }
}
