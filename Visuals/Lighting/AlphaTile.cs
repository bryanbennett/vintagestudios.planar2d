﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.Lighting
{
    public class AlphaTile
    {
        private Vector2 renderPosition;
        private Texture2D tile;
        private Point tilePosition;
        private Point lastPass;
        private TileBasedLightingSystem sys;
        private Rectangle rect;
        private float alpha = 1.0f;
        private float modAlpha = 1.0f;
        private int tileSize;

        //properties

        public Point TilePosition
        {
            get
            {
                return tilePosition;
            }
        }
        public float Alpha
        {
            get
            {
                return modAlpha;
            }
            set
            {
                modAlpha = value;
            }
        }

        public AlphaTile(Point tilePosition, Texture2D tile, TileBasedLightingSystem sys, int tileSize)
        {
            this.tilePosition = tilePosition;
            this.tile = tile;
            this.sys = sys;
            this.tileSize = tileSize;
            lastPass = new Point(-1, -1);
            renderPosition = new Vector2(tilePosition.X * tileSize, tilePosition.Y * tileSize);
            rect = new Rectangle((int)renderPosition.X, (int)renderPosition.Y, tileSize, tileSize);
        }
        public bool SetAlpha(Point lastPass, float alpha)
        {
            if (this.lastPass.X == lastPass.X && this.lastPass.Y == lastPass.Y)
            {
                return false;
            }
            else if (this.lastPass.X == -1 && this.lastPass.Y == -1)
            {
                this.alpha = alpha;
                this.modAlpha = alpha;
                this.lastPass = lastPass;
                return true;
            }
            else
            {
                if (alpha < Alpha)
                {
                    this.alpha = alpha;
                    this.modAlpha = alpha;
                }
                this.lastPass = lastPass;
                return true;
            }
        }
        public void DrawAt(SpriteBatch batch, float offsetX, float offsetY)
        {
            float x = renderPosition.X - offsetX;
            float y = renderPosition.Y - offsetY;
            rect.X = (int) x;
            rect.Y = (int) y;
            batch.Draw(tile, rect, Color.White * (modAlpha - sys.AmbientLight));
        }
        public void ModifyAlpha(float alpha)
        {
            modAlpha = alpha;
        }
        public void ResetAlpha()
        {
            modAlpha = this.alpha;
        }
    }
}
