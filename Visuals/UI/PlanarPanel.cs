﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace VintageStudios.Planar2D.Visuals.UI
{
    /// <summary>
    /// Class used as a container for VisualComponents.  Registers mouse click events, updates, and draws all VisualComponents.
    /// Mouse click events are sent to the VisualComponents contained within this container.
    /// </summary>
    public class PlanarPanel : VisualComponent
    {
        private List<VisualComponent> components;
        private int count;
        private bool needsReset;
        private PlanarWindow parent;
        private bool hasParent;

        public PlanarPanel(Rectangle bounds, GraphicsDevice device)
            :base(bounds, device)
        {
            components = new List<VisualComponent>();
            Visible = true;
        }
        /// <summary>
        /// Use this constructor if you plan on attaching this panel to a PlanarWindow.
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="parent"></param>
        public PlanarPanel(Rectangle bounds, PlanarWindow parent)
            :base(bounds, parent.Graphics)
        {
            components = new List<VisualComponent>();
            this.parent = parent;
            hasParent = true;
            Visible = true;
        }
        /// <summary>
        /// Adds a VisualComponent to this PlanarPanel.  Draw and update calls are determined
        /// by the order in which components are added to this panel.  Mouse order (when checking mouse events)
        /// are in reverse order, meaning the last component added is the first to be checked in Mouse Order.
        /// </summary>
        /// <param name="component"></param>
        public void AddComponent(VisualComponent component)
        {
            component.Parent = this;
            components.Add(component);
            count = components.Count;
        }
        /// <summary>
        /// Removes the specific component in this PlanarPanel.
        /// </summary>
        /// <param name="component"></param>
        public void RemoveComponent(VisualComponent component)
        {
            components.Remove(component);
            count = components.Count;
        }
        public override void Draw(SpriteBatch batch)
        {
            foreach (VisualComponent entry in components)
            {
                entry.Draw(batch);
            }
        }

        public override void Update(int delta)
        {
            foreach (VisualComponent entry in components)
            {
                Rectangle rect = new Rectangle(entry.Position.X + Bounds.X, entry.Position.Y + Bounds.Y, entry.Bounds.Width, entry.Bounds.Height);
                entry.Bounds = rect;
                entry.Update(delta);             
            }
        }

        public override void onClick(MouseState mouseState)
        {
            if (Visible)
            {
                needsReset = true;
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        entry.onClick(mouseState);
                    }
                    else
                    {
                        entry.Reset();
                    }
                }
            }
        }

        public override void onRelease(MouseState mouseState)
        {
            if (Visible)
            {
                needsReset = true;
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        entry.onRelease(mouseState);
                    }
                    else
                    {
                        entry.Reset();
                    }
                }
            }
        }

        public override void onDrag(MouseState mouseState)
        {
            if (Visible)
            {
                needsReset = true;
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        entry.onDrag(mouseState);
                    }
                    else
                    {
                        entry.Reset();
                    }
                }
            }
        }
        public override void onHover(MouseState mouseState)
        {
            if (Visible)
            {
                bool foundThisInstance = false;
                needsReset = true;
                
                for (int i = count - 1; i >= 0; i--)
                {
                    VisualComponent entry = components[i];
                    if (entry.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        entry.onHover(mouseState);
                        if (hasParent)
                        {
                            parent.ComponentFound = true;
                            foundThisInstance = true;
                        }
                    }
                    else
                    {
                        entry.Reset();
                    }
                }
                if (hasParent)
                {
                    if (!foundThisInstance)
                    {
                        parent.ComponentFound = false;
                    }
                }
            }
        }
        public override void Reset()
        {
            if (needsReset)
            {
                foreach (VisualComponent entry in components)
                {
                    entry.Reset();
                }
            }
        }

        //>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>
        /// <summary>
        /// Returns the number of components held in this container.
        /// </summary>
        public int Count { get { return count; } }


    }
}
