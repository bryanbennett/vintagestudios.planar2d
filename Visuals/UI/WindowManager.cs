﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public class WindowManager
    {
        private Dictionary<int , PlanarWindow> windows;
        private bool exclusive;

        //properties
        /// <summary>
        /// Gets or sets whether or not this WindowManager allows more than one window
        /// to be open at a time.  If it is set to true, the manager will make only one window
        /// visible at a time.  If it is set to false, more than one window can be visible.
        /// Default state is false.
        /// </summary>
        public bool ExclusiveWindows
        {
            get
            {
                return exclusive;
            }
            set
            {
                exclusive = value;
            }
        }

        public WindowManager()
        {
            windows = new Dictionary<int,PlanarWindow>();
        }
        public void AddWindow(PlanarWindow window)
        {
            windows.Add(window.WindowID, window);
        }
        public void RemoveWindow(PlanarWindow window)
        {
            windows.Remove(window.WindowID);
        }
        public void ShowWindow(int windowID)
        {
            if (exclusive)
            {
                HideAll();
            }
            windows[windowID].Visible = true;
        }
        public void HideWindow(int windowID)
        {
            windows[windowID].Visible = false;
        }
        public void HideAll()
        {
            foreach (KeyValuePair<int, PlanarWindow> entry in windows)
            {
                entry.Value.Visible = false;
            }
        }
        public void ToggleWindow(int windowID)
        {
            PlanarWindow window = windows[windowID];
            if (window.Visible)
            {
                window.Visible = false;
            }
            else
            {
                if (exclusive)
                {
                    HideAll();
                }
                window.Visible = true;
            }
        }
        public void DrawWindows(SpriteBatch batch)
        {
            foreach (KeyValuePair<int, PlanarWindow> entry in windows)
            {
                entry.Value.Draw(batch);
            }
        }
    }
}
