﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public class PlanarLabel : VisualComponent
    {
        private string text = "";
        private SpriteFont font;
        private Vector2 fontPos;
        private Color color;

        public PlanarLabel(Vector2 position, SpriteFont font, GraphicsDevice device)
            :base(new Rectangle((int)position.X, (int)position.Y, 0,0), device)
        {
            color = Color.Black;
            fontPos = position;
            this.font = font;
        }


        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            batch.DrawString(font, text, fontPos, color);
        }

        public override void Update(int delta)
        {
            fontPos = new Vector2(Bounds.X, Bounds.Y);
        }

        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void Reset()
        {
            
        }




        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        /// <summary>
        /// Sets the text of this PlanarLabel.  Default text is "".
        /// </summary>
        public string Text { get { return text; } set { text = value; }}
        /// <summary>
        /// Sets the color of this PlanarLabel.  Default color is black.
        /// </summary>
        public Color Color { set { color = value; } }
    }
}
