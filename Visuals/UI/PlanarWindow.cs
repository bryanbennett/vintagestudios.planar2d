﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using VintageStudios.Planar2D.GameStructure;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public class PlanarWindow : VisualComponent
    {
        private PlanarPanel panel;
        private GameState parent;
        private Color borderColor;
        private Texture2D borderTexture;
        private Texture2D backTexture;
        private int borderWidth;
        private Rectangle borderLeft;
        private Rectangle borderRight;
        private Rectangle borderUp;
        private Rectangle borderDown;
        private Rectangle exit;
        private Color backgroundColor;
        private bool componentFound;
        private bool isAnchored;
        private bool isDragging;
        private Point dragPoint;
        private MouseState oldState;
        private int exitWidth = 13;
        private int windowID;

        public PlanarWindow(Rectangle bounds, GameState parent, int windowID, GraphicsDevice device)
            :base(bounds, device)
        {
            this.windowID = windowID;
            panel = new PlanarPanel(bounds, this);
            this.parent = parent;
            borderWidth = 1;
            borderColor = Color.DarkGray;
            backgroundColor = Color.Black;
            var b = new Texture2D(parent.Controller.Game.GraphicsDevice, 1, 1);
            b.SetData(new Color[] { backgroundColor });
            backTexture = b;

            //default border texture
            var t = new Texture2D(parent.Controller.Game.GraphicsDevice, 1, 1);
            t.SetData(new Color[] { borderColor });
            borderTexture = t;
            borderLeft = new Rectangle(Bounds.X, Bounds.Y, borderWidth, Bounds.Height);
            borderRight = new Rectangle(Bounds.X + Bounds.Width - borderWidth, Bounds.Y, borderWidth, Bounds.Height);
            borderUp = new Rectangle(Bounds.X, Bounds.Y, Bounds.Width, borderWidth);
            borderDown = new Rectangle(Bounds.X, Bounds.Y + Bounds.Height - borderWidth, Bounds.Width, borderWidth);
            exit = new Rectangle(Bounds.X + Bounds.Width - exitWidth, Bounds.Y, exitWidth, exitWidth);
        }
        public void AddComponent(VisualComponent component)
        {
            panel.AddComponent(component);
        }
        public void RemoveComponent(VisualComponent component)
        {
            panel.RemoveComponent(component);
        }
        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            if (Visible)
            {
                batch.Draw(backTexture, Bounds, backgroundColor);
                panel.Draw(batch);
                batch.Draw(borderTexture, borderLeft, borderColor);
                batch.Draw(borderTexture, borderRight, borderColor);
                batch.Draw(borderTexture, borderUp, borderColor);
                batch.Draw(borderTexture, borderDown, borderColor);
                batch.Draw(borderTexture, exit, borderColor);
            }
        }

        public override void Update(int delta)
        {
            if (Visible)
            {
                panel.Update(delta);
            }
        }

        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Visible)
            {
                if (exit.Contains(mouseState.X, mouseState.Y))
                {
                    Visible = false;
                }
                else
                {
                    panel.onClick(mouseState);
                }
            }
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Visible && !isDragging)
            {
                panel.onRelease(mouseState);
            }
            else if (isDragging)
            {
                isDragging = false;
            }
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Visible && !isDragging)
            {
                panel.onDrag(mouseState);
            }
            if (!componentFound && Visible && !Anchored)
            {
                if (!isDragging)
                {
                    oldState = mouseState;
                    isDragging = true;
                    dragPoint = new Point(oldState.X - Bounds.X, oldState.Y - Bounds.Y);
                }
                else
                {
                    int dx = mouseState.X - dragPoint.X;
                    int dy = mouseState.Y - dragPoint.Y;
                    Rectangle rect = new Rectangle(dx,dy, Bounds.Width, Bounds.Height);
                    Bounds = rect;
                    borderLeft = new Rectangle(Bounds.X, Bounds.Y, borderWidth, Bounds.Height);
                    borderRight = new Rectangle(Bounds.X + Bounds.Width - borderWidth, Bounds.Y, borderWidth, Bounds.Height);
                    borderUp = new Rectangle(Bounds.X, Bounds.Y, Bounds.Width, borderWidth);
                    borderDown = new Rectangle(Bounds.X, Bounds.Y + Bounds.Height - borderWidth, Bounds.Width, borderWidth);
                    exit = new Rectangle(Bounds.X + Bounds.Width - exitWidth, Bounds.Y, exitWidth, exitWidth);
                    panel.Bounds = rect;
                }
            }
        }
        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Visible)
            {
                panel.onHover(mouseState);
            }
        }
        public override void Reset()
        {
            isDragging = false;
            panel.Reset();
        }



        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>

        /// <summary>
        /// Returns the window ID of this window.
        /// </summary>
        public int WindowID { get { return windowID; } }

        public PlanarPanel Panel { get { return panel; } }
        /// <summary>
        /// Sets the color of the border of this PlanarWindow.  Default is dark gray.
        /// </summary>
        public Color BorderColor
        {
            set
            {
                borderColor = value;
                var t = new Texture2D(parent.Controller.Game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                t.SetData(new Color[] { borderColor });
                borderTexture = t;
            }
        }
        /// <summary>
        /// Sets the background color of this window.  Default is black.
        /// </summary>
        public Color BackgroundColor
        {
            set
            {
                backgroundColor = value;
                var t = new Texture2D(parent.Controller.Game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                t.SetData(new Color[] { backgroundColor });
                backTexture = t;
            }
        }
        /// <summary>
        /// Sets the width of the PlanarWindow's borders.  Default is 1 pixel.
        /// </summary>
        public int BorderWidth
        {
            set
            {
                borderWidth = value;
                borderLeft = new Rectangle(Bounds.X, Bounds.Y, borderWidth, Bounds.Height);
                borderRight = new Rectangle(Bounds.X + Bounds.Width - borderWidth, Bounds.Y, borderWidth, Bounds.Height);
                borderUp = new Rectangle(Bounds.X, Bounds.Y, Bounds.Width, borderWidth);
                borderDown = new Rectangle(Bounds.X, Bounds.Y + Bounds.Height - borderWidth, Bounds.Width, borderWidth);
                exit = new Rectangle(Bounds.X + Bounds.Width - exitWidth, Bounds.Y, exitWidth, exitWidth);
            }
        }
        /// <summary>
        /// Gets or sets whether or not this window is anchored to a point.  If it is, it cannot be dragged.
        /// </summary>
        public bool Anchored { get { return isAnchored; } set { isAnchored = value; } }


        /// <summary>
        /// Returns whether or not a click event has registered a component contained in this window.  Otherwise, the window reacts to
        /// the click event instead of children components.
        /// </summary>
        public bool ComponentFound { get { return componentFound; } set { componentFound = value; } }
    }
}
