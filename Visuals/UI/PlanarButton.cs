﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public class PlanarButton : VisualComponent
    {
        private bool hasDelegate;
        private EventHandler clickEvent;
        private SpriteFont font;
        private Vector2 fontPos;
        private Vector2 fontOrigin;
        private Vector2 fontMeas;
        private Vector2 boxOrigin;
        private Texture2D borderTextureNorm;
        private Texture2D borderTextureClick;
        private Texture2D currentTexture;
        private Texture2D backTexture;
        private Color c;
        private Color currentColor;
        private string label = "";

        public PlanarButton(Rectangle bounds, SpriteFont font, Color c, GraphicsDevice device)
            :base(bounds, device)
        {
            this.font = font;
            borderTextureNorm = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            borderTextureClick = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            backTexture = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            fontMeas = font.MeasureString(label);
            fontOrigin = fontMeas / 2;
            boxOrigin = new Vector2(bounds.X + (bounds.Width / 2), bounds.Y + (bounds.Height / 2));
            fontPos = new Vector2(boxOrigin.X - fontOrigin.X, boxOrigin.Y - fontOrigin.Y);
            this.c = c;
            currentColor = c;
            this.borderTextureNorm.SetData(new Color[] { c });
            this.borderTextureClick.SetData(new Color[] { Color.White });
            backTexture.SetData(new Color[] { Color.Black });
            currentTexture = borderTextureNorm;
        }
        public void RegisterEvent(EventHandler method)
        {
            clickEvent = method;
            hasDelegate = true;
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(backTexture, Bounds, Color.White);
            batch.DrawString(font, label, fontPos, currentColor, 0, fontOrigin, 1.0f, SpriteEffects.None, 0.5f);
            Rectangle rect = new Rectangle(Bounds.X, Bounds.Y, 1, Bounds.Height);
            batch.Draw(currentTexture, rect, Color.White);
            rect = new Rectangle(Bounds.X, Bounds.Y, Bounds.Width, 1);
            batch.Draw(currentTexture, rect, Color.White);
            rect = new Rectangle(Bounds.Width - 1 + Bounds.X, Bounds.Y, 1, Bounds.Height);
            batch.Draw(currentTexture, rect, Color.White);
            rect = new Rectangle(Bounds.X, Bounds.Height - 1 + Bounds.Y, Bounds.Width, 1);
            batch.Draw(currentTexture, rect, Color.White);
        }

        public override void Update(int delta)
        {
            boxOrigin = new Vector2(Bounds.X + (Bounds.Width / 2), Bounds.Y + (Bounds.Height / 2));
            fontPos = new Vector2(boxOrigin.X, boxOrigin.Y);
        }

        public override void onClick(MouseState mouseState)
        {
            currentTexture = borderTextureClick;
            currentColor = Color.White;
            ClickEvent();
        }

        public override void onRelease(MouseState mouseState)
        {
            //nothing
        }

        public override void onDrag(MouseState mouseState)
        {
            //
        }
        public override void onHover(MouseState mouseState)
        {
            currentTexture = borderTextureNorm;
            currentColor = c;
        }
        public override void Reset()
        {
            currentTexture = borderTextureNorm;
            currentColor = c;
        }
        private void ClickEvent()
        {
            if (hasDelegate)
            {
                clickEvent(this, null);
            }
        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public string Label
        {
            get
            {
                return label;
            }
            set
            {
                label = value;
                fontMeas = font.MeasureString(label);
                fontOrigin = fontMeas / 2;
                fontPos = new Vector2(boxOrigin.X - fontOrigin.X, boxOrigin.Y - fontOrigin.Y);
            }
        }
    }
}
