﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public class PlanarImage : VisualComponent
    {
        private Texture2D currentImage;
        private Texture2D hoverImage;
        private Texture2D defaultImage;
        private Texture2D selectImage;

        public PlanarImage(Rectangle bounds, GraphicsDevice device, Texture2D[] images)
            :base(bounds, device)
        {
            this.currentImage = images[0];
            hoverImage = images[1];
            defaultImage = images[0];
            selectImage = images[2];
        }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            batch.Draw(currentImage, Bounds, Color.White);
        }

        public override void Update(int delta)
        {
            
        }

        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Bounds.Contains(mouseState.X, mouseState.Y))
            {
                currentImage = selectImage;
            }
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Bounds.Contains(mouseState.X, mouseState.Y))
            {

            }
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {

        }

        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Bounds.Contains(mouseState.X, mouseState.Y))
            {
                currentImage = hoverImage;
            }
        }

        public override void Reset()
        {
            currentImage = defaultImage;
        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public Texture2D HoverImage
        {
            get
            {
                return hoverImage;
            }
            set
            {
                hoverImage = value;
            }
        }
        public Texture2D DefaultImage
        {
            get
            {
                return defaultImage;
            }
            set
            {
                defaultImage = value;
            }
        }
        public Texture2D SelectedImage
        {
            get
            {
                return selectImage;
            }
            set
            {
                selectImage = value;
            }
        }
        public Texture2D CurrentImage
        {
            get
            {
                return currentImage;
            }
            set
            {
                currentImage = value;
            }
        }
    }
}
