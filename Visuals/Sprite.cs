﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace VintageStudios.Planar2D.Visuals
{
    public class Sprite
    {
        private Point bounds;
        private Point frameSize;
        private Point currentFrame;
        private Point tileSize;
        private Point sheetFrames = new Point(3, 4);
        private int timeSinceLastFrame = 0;
        private int animSpeed = 25;
        private Texture2D sprite;
        private Facing face = Facing.South;
        private bool continuousAnimation = false;
        private bool isMoving = false;
        private int direction = 1;

        /// <summary>
        /// Sets whether or not this sprite should have continuous animation.
        /// </summary>
        public bool ContinuousAnimation
        {
            set
            {
                continuousAnimation = value;
            }
        }
        /// <summary>
        /// Sets the animation speed of the sprite.
        /// </summary>
        public int AnimationSpeed
        {
            set
            {
                animSpeed = value;
            }
        }
        /// <summary>
        /// Sets the Facing value of the sprite and therefore determining which row of the spritesheet to use.
        /// </summary>
        public Facing Facing
        {
            set
            {
                face = value;
                switch (face)
                {
                    case Facing.South:
                        currentFrame.Y = 0;
                        break;
                    case Facing.West:
                        currentFrame.Y = 1;
                        break;
                    case Facing.East:
                        currentFrame.Y = 2;
                        break;
                    case Facing.North:
                        currentFrame.Y = 3;
                        break;
                }
            }
        }
        /// <summary>
        /// Sets whether or not the sprite is Moving.  Useless if continuous animation is set to true.
        /// </summary>
        public bool Moving
        {
            set
            {
                isMoving = value;
            }
        }
        /// <summary>
        /// Returns the framesize in pixels of invidual frames in a spritesheet.
        /// </summary>
        public Rectangle FrameSize
        {
            get
            {
                return new Rectangle(0, 0, frameSize.X, frameSize.Y);
            }
        }
        public Point TileSize
        {
            get
            {
                return tileSize;
            }
        }

        public Sprite(Texture2D sprite)
        {
            this.sprite = sprite;
            bounds = new Point(sprite.Width, sprite.Height);
            frameSize = new Point(sprite.Width / 3, sprite.Height / 4);
            tileSize = new Point(frameSize.X / 16, frameSize.Y / 16);
            currentFrame = new Point(0, 0);
        }
        /// <summary>
        /// Draws the sprite at a specific location in pixel space.
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void DrawAt(SpriteBatch batch, int x, int y)
        {
            Rectangle r = new Rectangle(currentFrame.X * frameSize.X, currentFrame.Y * frameSize.Y, frameSize.X, frameSize.Y);
            Vector2 renderPosition = new Vector2(x, y);
            batch.Draw(sprite, renderPosition, r, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        }
        /// <summary>
        /// Updates the sprite's animation.
        /// </summary>
        /// <param name="delta"></param>
        public void Update(int delta)
        {
            if (isMoving)
            {
                timeSinceLastFrame += delta;
                if (timeSinceLastFrame >= animSpeed)
                {
                    timeSinceLastFrame -= animSpeed;
                    currentFrame.X += direction;
                    if (currentFrame.X >= sheetFrames.X || currentFrame.X < 0)
                    {
                        direction *= -1;
                        if (direction > 0)
                        {
                            currentFrame.X = 0;
                        }
                        else
                        {
                            currentFrame.X = sheetFrames.X - 1;
                        }
                    }
                }
            }
            else if (!isMoving && continuousAnimation)
            {
                timeSinceLastFrame += delta;
                if (timeSinceLastFrame >= animSpeed)
                {
                    timeSinceLastFrame -= animSpeed;
                    currentFrame.X += direction;
                    if (currentFrame.X >= sheetFrames.X || currentFrame.X < 0)
                    {
                        direction *= -1;
                        if (direction > 0)
                        {
                            currentFrame.X = 0;
                        }
                        else
                        {
                            currentFrame.X = sheetFrames.X - 1;
                        }
                    }
                }
            }

        }
    }
}
