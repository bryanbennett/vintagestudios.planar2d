﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using VintageStudios.Planar2D.Input;
using Microsoft.Xna.Framework.Content;

namespace VintageStudios.Planar2D.GameStructure
{
    /// <summary>
    /// XNA Game component that manages a game's gamestates by drawing only the active gamestate and provides a way to update all gamestates.
    /// </summary>
    public class GameStateController : GameComponent
    {
        private Dictionary<int,GameState> states;
        private ContentManager content;
        private bool isLoading = true;

        //properties
        public bool Loading
        {
            get
            {
                return isLoading;
            }
            set
            {
                isLoading = value;
            }
        }

        public ContentManager Content
        {
            get
            {
                return content;
            }
        }

        public GameStateController(Game game)
            :base(game)
        {
            states = new Dictionary<int,GameState>();
            content = game.Content;
        }
        /// <summary>
        /// Adds a gamestate to the controller for drawing and updating.  The second argument should be set to "true" if it is to be the active gamestate.
        /// </summary>
        /// <param name="gamestate"></param>
        /// <param name="makeActiveState"></param>
        public void AddGameState(GameState gamestate, bool isVisible, bool hasFocus)
        {
            gamestate.Visible = isVisible;
            gamestate.Focus = hasFocus;
            states.Add(gamestate.StateID, gamestate);
        }
        /// <summary>
        /// Sets the focus to a gamestate based on state ID.  Disables the focus from all other gamestates.
        /// </summary>
        /// <param name="stateID"></param>
        public void SetFocus(int stateID)
        {
            foreach (KeyValuePair<int, GameState> entry in states)
            {
                if (entry.Key == stateID)
                {
                    entry.Value.Focus = true;
                }
                else
                {
                    entry.Value.Focus = false;
                }
            }
        }
        public void SetStateVisibility(int stateID, bool isVisible)
        {
            states[stateID].Visible = isVisible;
        }
        public GameState GetState(int stateID)
        {
            return states[stateID];
        }
        public void Draw(SpriteBatch batch)
        {
            if (Loading)
            {
                return;
            }
            foreach (KeyValuePair<int, GameState> entry in states)
            {
                if (entry.Value.Visible)
                {
                    entry.Value.Draw(batch);

                    batch.Begin();
                        entry.Value.WindowManager.DrawWindows(batch);
                    batch.End();
                }
            }
        }
        /// <summary>
        /// Updates all of the gamestates added to this controller.
        /// </summary>
        /// <param name="time"></param>
        public override void Update(GameTime time)
        {
            int delta = time.ElapsedGameTime.Milliseconds;
            if (Loading)
            {
                return;
            }
            foreach (KeyValuePair<int,GameState> entry in states)
            {
                entry.Value.Update(delta);
                entry.Value.UpdateTriggers(delta);
                if (entry.Value.Focus)
                {
                    if (!entry.Value.KeyInputDisabled)
                    {
                        entry.Value.KeyInput(delta);
                    }
                    entry.Value.MouseHandler.Update(delta);
                }
            }
        }
    }
}
